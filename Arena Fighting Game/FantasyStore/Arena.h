//
// Created by Sarah Sanders on 12/2/16.
//

#ifndef FANTASYSTORE_ARENA_H
#define FANTASYSTORE_ARENA_H
#include "Enemy.h"
#include "vector"
#include "Player.h"

class Arena {
    protected:
        vector<Enemy*> EnemyVector;
        Player* thePlayer = Player::instance();
    public:

        /*
         * Iterate through vector Enemy
         * If vector is empty then
         * exit code.
         * If there are enemies still contained
         * in the vector, then return 1
         */
        bool checkEnemy();


        /*
         * Add instance of enemy into EnemyVector
         */
        void addEnemy(Enemy* enemy);


        /*
         * Remove instance of enemy from EnemyVector
         */
        void removeEnemy();



        /*
         * DO I NEED THIS???
         */
        vector<Enemy*> getEnemy();




        /*
         *destructor
         */
        ~Arena();
};


#endif //FANTASYSTORE_ARENA_H
