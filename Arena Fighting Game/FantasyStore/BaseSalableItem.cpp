//
// Created by Sarah Sanders on 11/2/16.
//

#include "BaseSalableItem.h"
#include <iostream>
using namespace std;

//default constructor that will set the Type, Name, and cost of the salable item
BaseSalableItem::BaseSalableItem()
{
    //itemType is an empty string
    this->itemType="";
    //itemName is an empty string
    this->itemName="";
    //itemDecription is an empty string
    this->itemDescription="";
    //itemCost is 0
    this->cost=0;
    //itemStat is 0
    this->Stat=0;
}

//set the constructors to their name
BaseSalableItem::BaseSalableItem(string itemType, string itemName, string itemDescription, int cost, int Stat)
{
    //itemType is type string
    this->itemType=itemType;
    //itemName is type string
    this->itemName=itemName;
    //itemDescription is type string
    this->itemDescription=itemDescription;
    //itemCost is type int
    this->cost=cost;
    //itemStat is type int
    this->Stat=Stat;
}


string BaseSalableItem ::getType()
{
    //return item type
    return itemType;
}

string BaseSalableItem ::getName()
{
    //return item Name
    return itemName;
}

string BaseSalableItem ::getitemDescription()
{
    //return item Description
    return itemDescription;
}


int BaseSalableItem ::getCost()
{
    //return item cost
    return cost;
}

int BaseSalableItem ::getStat()
{
    //return item Stat
    return Stat;
}