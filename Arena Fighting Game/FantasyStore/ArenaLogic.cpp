//
// Created by Sarah Sanders on 12/2/16.
//

#include "ArenaLogic.h"
#include <ctime>
#include <cstdlib>



void ArenaLogic::promptArenaInput()
{
    //configureEnemyMove();
    cout<<"######################"<<endl;
    cout<<"| You are in Battle! | "<<endl;
    cout<<"###################### "<<endl;
    cout<<" "<<endl;
    cout<<"Your Opponent is Gym Leader "<<EnemyVector[0]->getName()<<endl;
    cout<<" "<<endl;
    cout<<"What do you want your Pokemon to do? "<<endl;

    cout << " 'a' = Attack"<< endl;
    cout << " 'b' = Defend" << endl;
    cout << " 'c' = Restore" << endl;
    cout << " 'd' = View Pokemon Stats" << endl;
    cout<<" "<<endl;
    cout<<" "<<endl;
    cout<<" "<<endl;
    cout<<" "<<endl;

    //validuserInput = 0
    int validuserInput = 0;

    while(validuserInput == 0)
    {
        //validate the user input.
        cin >> userInput;
        //if user input is 'a', 'b', 'c', 'd', or 'e'
        if (userInput == "a" or userInput =="b" or userInput =="c" or userInput=="d" or userInput =="e")
        {
            //user input = 1
            validuserInput = 1;
        }
        else
        {
            //if user input is anything else then print "Bad Input"
            cout << "Bad Input"<<endl;
        }

    }

    //if user input = 'a'
    if (userInput == "a")
    {
        //start beginAttackLogic()
        beginAttackLogic();
    }
    //if user input = 'b'
    if (userInput == "b")
    {
        //start beginDefendLogic()
        beginDefendLogic();
    }
    //if user input = 'c'
    if (userInput == "c")
    {
        //start beginRestoreLogic()
        beginRestoreLogic();
    }
    //if user input = 'd'
    if (userInput == "d")
    {
        //start PrintPlayerStatsLogic()
        PrintPlayerStatsLogic();
    }

}



void ArenaLogic::getEnemyList()
{
    //iterate through EnemyVector size
    for(int i=0; i<EnemyVector.size();i++)
    {
        //print name, health, attack, defense and balance of Enemy
        cout<<""<<endl;
        cout<<"     "<< "Gym Leader "<<EnemyVector[i]->getName()<<"'s Pokemon Stats:"<<endl;
        cout<<"             "<< EnemyVector[i]->getHealth()<<" Health Points"<<endl;
        cout<<"             "<< EnemyVector[i]->getAttack()<<" Attack Power"<<endl;
        cout<<"             "<< EnemyVector[i]->getDefense()<<" Defense Power"<<endl;
        cout<<"             "<< EnemyVector[i]->getBalance()<<" PokeDollars"<<endl;
        cout<<""<<endl;
    }
}





void ArenaLogic::beginAttackLogic()
{
    cout<<thePlayer->getname()<<"'s Pokemon attacked!"<<endl;
    cout<<" "<<endl;
    //if the Enemy is attacking
    if(EnemyVector[0]->getAttackState()==1)
    {
        //subtract the Player's Attack Value from the Enemy's health
        EnemyVector[0]->removeEnemyHealth(thePlayer->getPlayerAttackValue());

        //subtract the Enemy's Attack Value from the Player's health
        thePlayer->removePlayerhealth(EnemyVector[0]->getAttack());

        //print that the Enemy attacked
        cout<< EnemyVector[0]->getName()<<"'s Pokemon used an Attack Move! It did "<<EnemyVector[0]->getAttack()<<" damage"<<endl;
        cout<< ""<<endl;
    }
    else
    {
        //if the Enemy is defending then check if the Player Attack Value - Enemy Defense Value > 0
        if (thePlayer->getPlayerAttackValue()-EnemyVector[0]->getDefense()>0)
        {
            //subtract the Enemy's health using the parameter of the Player's Attack Value - Enemy's Defense Value
            EnemyVector[0]->removeEnemyHealth(thePlayer->getPlayerAttackValue()-EnemyVector[0]->getDefense());

            //print the damage done to the Enemy
            cout<< EnemyVector[0]->getName()<<" used a Defense Move. You only did "<<thePlayer->getPlayerAttackValue()-EnemyVector[0]->getDefense()<<" damage."<<endl;
        }
        //if the Enemy is defending then check if the Player Attack Value - Enemy Defense Value <= 0
        else if(thePlayer->getPlayerAttackValue()-EnemyVector[0]->getDefense()<=0)
        {
            //subtract the Enemy's health by 5
            EnemyVector[0]->removeEnemyHealth(5);

            //print 5 damage done to Enemy
            cout<< EnemyVector[0]->getName()<<"'s Pokemon used a Defense Move. Their Defense Value is greater than your Attack Value, so you only dealt 5 damage."<<endl;
        }
        cout<<""<<endl;
    }
    //return to checkHealthStatus()
    return checkHealthStatus();
}

void ArenaLogic::beginDefendLogic()
{
cout<<thePlayer->getname()<<"'s Pokemon defended itself!"<<endl;

    //if the Enemy is attacking
    if(EnemyVector[0]->getAttackState()==1)
    {
        //check if the Enemy Attack Value - the Player's Defense Value > 0
        if (EnemyVector[0]->getAttack()-thePlayer->getPlayerDefendValue()>0)
        {
            //subtract from the Player's health using the parameter of Enemy Attack Value - the Player's Defense Value
            thePlayer->removePlayerhealth(EnemyVector[0]->getAttack()-thePlayer->getPlayerDefendValue());

            //print damage done to Player
            cout<< EnemyVector[0]->getName()<<"'s Pokemon used an Attack Move! It did "<<EnemyVector[0]->getAttack()-thePlayer->getPlayerDefendValue()<<" damage."<<endl;
        }
        //if Enemy is defending, then if the Enemy Attack Value - the Player's Defense Value <= 0
        else if(EnemyVector[0]->getAttack()-thePlayer->getPlayerDefendValue()<=0)
        {
            //remove 5 from Player Health
            thePlayer->removePlayerhealth(5);
            cout<< thePlayer->getname()<<"'s Pokemon Defense Value is greater than the opponent's Attack Value, so 5 damage was dealt to you."<<endl;
        }
    }
    else
    {
        //print no one receives damage
        cout<<"You and "<<EnemyVector[0]->getName()<< "'s Pokemon used a Defense Move. No one received any damage." <<endl;
    }
    //return to checkHealthStatus()
    return checkHealthStatus();
}






void ArenaLogic::beginRestoreLogic()
{
    //create a variable type bool for checking for Health item
    bool checkitem = 0;
    //while loop to establish the item type
    while (checkitem == 0)
    {
        //iterate through Bag to only print out the items that are type Health
        for (long i = 0; i < thePlayer->getBag().size(); i++)
        {
            //if item type is a Weapon then break out of while loop
            if (thePlayer->getBag()[i]->getType() == "Health")
            {
                //bool variable is true. break out of for loop
                checkitem = 1;
                break;
            }
        }
        //if item is not Health then return to promptArenaInput()
        if (checkitem == 0)
        {
            //No items to equip if checkitem = 0
            cout << "There are no Health items in your Bag" << endl;
            cout << "" << endl;
            return promptArenaInput();
        }
    }
    cout << "" << endl;
    cout << "Choose a Health item to restore your Pokemon " << endl;
    cout << "or CANCEL with 'x' " << endl;
    cout << "" << endl;
    //iterate through Bag
    for (int i = 0; i < thePlayer->getBag().size(); i++)
    {
        //if item is Health then print Name and Restore Value
        if (thePlayer->getBag()[i]->getType() == "Health")
        {
            cout << thePlayer->getBag()[i]->getName() << "   Restore Value: " << thePlayer->getBag()[i]->getStat()
                 << " Health Points" << endl;
        }
    }

    //variable of type intger to validate user input
    int validuserInput = 0;
    //while loop to validate user input
    while (validuserInput == 0)
    {
        //ask for user input
        cin >> userInput;
        //iterate through Bag to validate the user input
        for (int i = 0; i < thePlayer->getBag().size(); i++)
        {
            //create a variable for the Name of item in vector Bag
            string x = thePlayer->getBag()[i]->getName();
            //if the user input is valid add values of items to Player stats
            if (userInput == x)
            {
                //if item is type Health then add Stat to Player Health
                if (thePlayer->getBag()[i]->getType() == "Health")
                {

                    //add to Player Health by using parameters item Stat + Player Health
                    thePlayer->addPlayerhealth(thePlayer->getBag()[i]->getStat());
                    cout<< "Your Pokemon has been added "<<thePlayer->getBag()[i]->getStat()<<" Health Points"<<endl;
                    cout<< " "<<endl;
                    thePlayer->removeitemfromBag(i);

                    //user input is valid
                    validuserInput = 1;
                    cout << " " << endl;

                    //break out of for loop
                    break;
                }
            }

        }
        //if user wants to cancel then return to promptArenaInput()
        if (userInput == "x")
        {
            //return to promptArenaInput()
            cout << " " << endl;
            return promptArenaInput();
        }
        //if user input is invalid then print Bad Input
        if (validuserInput == 0)
        {
            cout << "Bad input" << endl;
        }

    }

    //if the Enemy is attacking
    if(EnemyVector[0]->getAttackState()==1)
    {
        //subtract the Enemy's Attack Value from the Player's health
        thePlayer->removePlayerhealth(EnemyVector[0]->getAttack());

        //print that the Enemy attacked
        cout<< EnemyVector[0]->getName()<<"'s Pokemon used an Attack Move! It did "<<EnemyVector[0]->getAttack()<<" damage"<<endl;
        cout<< ""<<endl;
    }
    //if the Enemy is defending
    else
    {
        //print no one receives damage
        cout<<EnemyVector[0]->getName()<< "'s Pokemon used a Defense Move. No one received any damage." <<endl;
    }

        //return to checkHealthStatus()
        return checkHealthStatus();

}




void ArenaLogic::PrintPlayerStatsLogic()
{
    //call print stats function from Player.h
    thePlayer->printStats();

    //return to promptArenaInput()
    return promptArenaInput();
}



void ArenaLogic::checkHealthStatus()
{
    //if the Player's health is greater than 0
    if(thePlayer->gethealth()>0)
    {
        //if Enemy's Health is greater than 0
        if(EnemyVector[0]->getHealth()>0)
        {
            cout<<EnemyVector[0]->getName()<<"'s Pokemon has "<<EnemyVector[0]->getHealth()<<" Health Points"<<endl;
            cout<<" "<<endl;
            cout<<" "<<endl;
            cout<<" "<<endl;
            //return configureEnemyMove()
            return configureEnemyMove();
        }
        //if Enemy's Health is less than or equal to 0
        else if(EnemyVector[0]->getHealth()<=0)
        {
            //print that Player has defeated Enemy
            cout<< thePlayer->getname()<<" has defeated Gym Leader "<< EnemyVector[0]->getName()<<"."<<endl;

            //print the balance won by Player
            cout<<thePlayer->getname()<< " has received "<< EnemyVector[0]->getBalance()<< " PokeDollars for winning."<<endl;

            //print Pokemon is restore to 50 Health Points
            cout<<thePlayer->getname()<< "'s Pokemon has been restored back to 50 Health Points."<< endl;
            cout<<" "<<endl;
            cout<<" "<<endl;
            cout<<" "<<endl;
            cout<<" "<<endl;

            //add Player balance by Enemy balance
            thePlayer->addPlayerbalance(EnemyVector[0]->getBalance());

            //remove Enemy from EnemyVector
            removeEnemy();

            //set Player health to 50
            thePlayer->setPlayerHealth(50);
        }
    }
    else
    {
        //print Player lost to Enemy
        cout<<"You lost to Gym Leader "<<EnemyVector[0]->getName()<<endl;
        thePlayer->setPlayerHealth(50);
    }


}

void ArenaLogic::configureEnemyMove()
{
    //reset both Attack and Defense States to 0
    EnemyVector[0]-> setAttackState(0);
    EnemyVector[0]-> setDefendState(0);
    //call random time
    srand(time(0));
    //set random integer
    int randomval = rand()%2;
    //if random value = 0
    if (randomval==0)
    {
        //set Enemy Attack State = 1
        EnemyVector[0]->setAttackState(1);
    }
    //if random value = 1
    else if(randomval==1)
    {
        //set EnemyDefend State = 1
        EnemyVector[0]->setDefendState(1);
    }

    //return to promptArenaInput()
    return promptArenaInput();
}