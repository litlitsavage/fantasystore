//
// Created by Sarah Sanders on 12/2/16.
//

#ifndef FANTASYSTORE_ARENALOGIC_H
#define FANTASYSTORE_ARENALOGIC_H
#include <iostream>
using namespace std;
#include "Arena.h"

/*
 * This file contains the Logic needed for the Player and the Enemy to fight in the Arena.
 * It will call on Attack, Defend, Restore, and Print Stat functions that the user will
 * have the choice of using. CheckHealthStatus function will determine who won the fight
 * in the arena and whether or not the Player has more Enemies to fight. ConfigureEnemyMove
 * randomly determines if the Enemy will Attack or Defend itself.
 */

class ArenaLogic:public Arena {
    private:
        string userInput;
    public:

        /*
         * This method will be the main menu of the arena.
         * It will print the Player's opponent, and the options that
         * the Player has for the Logic of the Arena. These options
         * include Attack, Defend, Restore, and View Pokemon Stats.
         * The function will validate the user input and continue the
         * Logic with the correct input and print "Bad Input" to prompt
         * the user again.
         *
         */
        void promptArenaInput();



        /*
         * Iterate through the EnemyVector and print out
         * the Enemy's name, health, attack, defense and balance.
         * Return to promptArenaInput()
         */
        void getEnemyList();


        /*
         * Check to see if the Enemy is attacking or defending.
         * If the Enemy is attacking, then subtract Player Attack Value
         * from Enemy health, and subtract Enemy Attack Value from Player
         * health. If Enemy is defending then check if Player's Attack Value
         * is greater than Enemy's Defense Value. If it is, then remove
         * Enemy health by using the parameters Player's Attack Value minus
         * Enemy's Defense Value. If Enemy's Defense Value is greater than the
         * Player's Attack Value, remove 5 from Enemy health.
         * Return to checkHealthStatus()
         */
        void beginAttackLogic();




        /*
         * Check to see if the Enemy is attacking or defending.
         * If the Enemy is attacking, and Player Defense Value > Enemy
         * Attack Value, then subtract Player health using the
         * parameters Enemy Attack Value - Player Defend Value.
         * If Player Defense Value < Enemy Attack Value, then remove 5
         * from Player's health. If the Enemy defends, then no one receives
         * any damage.
         * Return to checkHealthStatus()
         */
        void beginDefendLogic();





        /*
         * Check to see if any items in Player's Bag are of type "Health". If yes, then
         * continue, and if not then print "There are no items to restore your Pokemon.
         * Print out all Health items in Bag. Ask for user input. If user input= item name
         * or 'x' then user input is valid. Otherwise print "Bad Input". If user input
         * equals 'x' then return to promptArenaInput(). If user input equals an item name
         * of type Health, then add Stat to Player health and remove item from Bag.
         * Check to see if Enemy is attacking or defending. If Enemy is attacking
         * subtract Enemy Attack Value from Player health. If Enemy is defending then print
         * that no damage is done.
         * return to checkHealthStatus()
         */
        void beginRestoreLogic();



        /*
         * This method will allow the Player to view their stats
         */
        void PrintPlayerStatsLogic();

        /*
         * If both the Player and Enemy's health is greater than 0, then
         * return configureEnemyMove(). If Enemy's health is less than 0
         * while the Player's is not, then add to the Player's balance the
         * parameter Enemy balance. Remove Enemy from the EnemyVector, and
         * reset the Player's Health to 50. If the Player loses to Enemy, print
         * "Player lost", reset Player health to 50 and return to promptArenaInput();
         */
        void checkHealthStatus();



        /*
         * create a random value using srand. If the value = 0
         * then AttackState for Enemy is true. if the value = 1
         * then DefenseStae for Enmey is true.
         * return promptArenaInput()
         */
        void configureEnemyMove();

};


#endif //FANTASYSTORE_ARENALOGIC_H
