//Sarah Sanders
//Fantasy Store
//CST 210
//12/11/2016






//  Created by Sarah Sanders  12/11/2016



#include <iostream>
#include <fstream>
#include<sstream>
#include "Weapon.h"
#include "Health.h"
#include "Armor.h"
#include "Store.h"
#include "StoreLogic.h"
#include "GameLogic.h"

using namespace std;
int main()
{
    //create instance of Game
    GameLogic* game1 = new GameLogic;
    //create instance of store
    StoreLogic* store1= new StoreLogic;
    //create instance of item
    BaseSalableItem *item;
    //create instance of enemy
    Enemy *enemy;



    //open Enemy in file
    ifstream Enemyinfile;


    try
    {
        Enemyinfile.open("./Enemy.txt", ios::in);
        if (!Enemyinfile)
        {
            Enemyinfile.close();

            throw  invalid_argument("Could not open enemy input file");
        }

    }

    catch (invalid_argument x)
    {
        //print exception message
        cout<<x.what()<<endl;
        return -1;
    }

    //parse Enemy file
    string token_p, token_o, token_u, token_y, token_t;
    string eline;
    while(getline(Enemyinfile,eline))
    {
        //create a string stream from the line and parse its tokens using a | delimeter
        istringstream lineStream(eline);
        getline(lineStream, token_p, '|');
        getline(lineStream, token_o, '|');
        getline(lineStream, token_u, '|');
        getline(lineStream, token_y, '|');
        getline(lineStream, token_t, '|');
        //add tokens to new item Enemy
        enemy = new Enemy(token_p, stoi(token_o), stoi(token_u), stoi(token_y),stoi(token_t));
        game1-> getArenaLogic()-> addEnemy(enemy);
    }
    //close file
    Enemyinfile.close();









///Users/sarah/Fantasy Store
    //open weapon file
    //read weapon file
    ifstream Weaponinfile;
    try
    {
        //open Weapon file
        Weaponinfile.open("./Weapons.txt", ios::in);
        if (!Weaponinfile)
        {
            Weaponinfile.close();

            throw  invalid_argument("Could not open Weapon input file");
        }

    }

    catch (invalid_argument x)
    {
        //print exception message
        cout<<x.what()<<endl;
        return -1;
    }
    //parse weapon file
    string token1, token2, token3, token4, token_a;
    string line;





    while(getline(Weaponinfile,line))
    {
        //create a string stream from the line and parse its tokens using a | delimeter
        istringstream lineStream(line);
        getline(lineStream, token1, '|');
        getline(lineStream, token2, '|');
        getline(lineStream, token3, '|');
        getline(lineStream, token4, '|');
        getline(lineStream, token_a, '|');




        try
        {
            //add tokens to new item Weapon
            item = BaseSalableItem::create(token1, token2, token3, stoi(token4),stoi(token_a));

            if (item == NULL)
            {
                throw invalid_argument("The type for one of your items is not 'Weapon'");
            }
            game1->getStoreLogic()->addSalableItem(item);

        }

        catch(invalid_argument x)
        {

            //Print exception message
            cout<<x.what()<<endl;
            return -1;
        }


    }
    //close file
    Weaponinfile.close();



    //open health file
    //read health file

    ifstream Healthinfile;


    try
    {
        //open Weapon file
        Healthinfile.open("./Health.txt", ios::in);
        if (!Healthinfile)
        {
            Healthinfile.close();

            throw  invalid_argument("Could not open Health input file");
        }

    }

    catch (invalid_argument x)
    {
        //print exception message
        cout<<x.what()<<endl;
        return -1;
    }



    //parse health file
    string token_b, token5, token6, token7, token8;
    string hline;
    while(getline(Healthinfile,hline))
    {
        //create a string stream from the line and parse its tokens using a | delimeter
        istringstream lineStream(hline);
        getline(lineStream, token_b, '|');
        getline(lineStream, token5, '|');
        getline(lineStream, token6, '|');
        getline(lineStream, token7, '|');
        getline(lineStream, token8, '|');


        try
        {
            //add tokens to new item Weapon
            item = BaseSalableItem::create(token_b, token5, token6, stoi(token7),stoi(token8));

            if (item == NULL)
            {
                throw invalid_argument("The type for one of your items is not 'Health'");
            }
            game1->getStoreLogic()->addSalableItem(item);

        }

        catch(invalid_argument x)
        {

            //Print exception message
            cout<<x.what()<<endl;
            return -1;
        }
    }
    //close file
    Healthinfile.close();










    //open armor file
    //read armor file
    ifstream Armorinfile;
    try
    {
        //open Armor file
        Armorinfile.open("./Armor.txt", ios::in);
        if (!Armorinfile)
        {
            Armorinfile.close();

            throw  invalid_argument("Could not open Armor input file");
        }

    }

    catch (invalid_argument x)
    {
        //print exception message
        cout<<x.what()<<endl;
        return -1;
    }



    //parse through armor file
    string token_c, token9, token10, token11, token12;
    string aline;
    while(getline(Armorinfile,aline))
    {
        //create a string stream from the line and parse its tokens using a | delimeter
        istringstream lineStream(aline);
        getline(lineStream, token_c, '|');
        getline(lineStream, token9, '|');
        getline(lineStream, token10, '|');
        getline(lineStream, token11, '|');
        getline(lineStream, token12, '|');



        try
        {
            //add tokens to new item Armor
            item = BaseSalableItem::create(token_c, token9, token10, stoi(token11),stoi(token12));

            if (item == NULL)
            {
                throw invalid_argument("The type for one of your items is not 'Armor'");
            }
            game1->getStoreLogic()->addSalableItem(item);

        }

        catch(invalid_argument x)
        {

            //Print exception message
            cout<<x.what()<<endl;
            return -1;
        }
    }
    //close file
    Armorinfile.close();
    //begin promptuserinput

    //game1->getStoreLogic()->promptStoreInput();
    game1->StartGameLogic();
    //cleanup and return
    delete game1;

    return 0;
}