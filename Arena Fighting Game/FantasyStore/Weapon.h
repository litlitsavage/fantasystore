//
// Created by Sarah Sanders on 11/4/16.
//

#ifndef FANTASYSTORE_WEAPON_H
#define FANTASYSTORE_WEAPON_H
#include "BaseSalableItem.h"
#include <iostream>
using namespace std;
/*
 * This class is an item Type and will hold all the constructors a Weapon needs.
 * This includes type, name, description, cost and stat
 */
class Weapon: public BaseSalableItem
{
    public:
    /*
     * create a constructor that will include the Weapon type, name, description, cost and stat
     */
        Weapon(string itemType, string itemName,string itemDescription, int cost, int Stat);
};


#endif //FANTASYSTORE_WEAPON_H
