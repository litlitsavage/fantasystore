//
// Created by Sarah Sanders on 12/2/16.
//

#include "Game.h"
#include <fstream>


void Game::save()
{
    cout<<"f"<<endl;
    ofstream saveOutFile;

    try
    {
        saveOutFile.open("./SaveFile.txt", ios::out);

        //if the armor input file cannot be opened
        if(saveOutFile)
        {
            saveOutFile.close();

            //throw exception to indicate that file cannot be opened
            throw invalid_argument("Could not open save file");
        }
    }

    catch(invalid_argument x)
    {
        //print exception message
        cout << x.what() <<endl;
        return;
    }
}

StoreLogic* Game::getStoreLogic()
{
    //return StoreLogic1
    return StoreLogic1;
}

ArenaLogic* Game::getArenaLogic()
{
    //return ArenaLogic1
    return ArenaLogic1;
}




Game::~Game()
{
    delete thePlayer;
    delete ArenaLogic1;
    delete StoreLogic1;
}
