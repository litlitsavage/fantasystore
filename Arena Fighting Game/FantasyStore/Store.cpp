//
// Created by Sarah Sanders on 11/5/16.
//

#include "Store.h"


vector<BaseSalableItem*> Store ::getcart()
{
    //return BaseSalableItem
    return cart;
}


bool Store ::checkcartsize()
{
    //if cart size is 0
    if (cart.size() == 0)
    {
        //print empty cart
        cout<<"Empty cart" <<endl;
        //return 0
        return 0;
    }
    //return 1
    return 1;
}

void Store::printcart()
{
    cout << "" << endl;
    cout<<"Items in Cart: " <<endl;
    //iterate through cart
    for(int i=0; i<cart.size();i++)
    {
        //print name of item in cart
        cout << cart[i]->getName()<<endl;
    }
}


Store::~Store()
{
    //iterate through cart
    for(int i=0; i<cart.size();i++)
    {
        //clean up
        delete cart[i];
    }
}
