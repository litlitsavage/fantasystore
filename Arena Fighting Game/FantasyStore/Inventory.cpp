//
// Created by Sarah Sanders on 11/5/16.
//

#include "Inventory.h"
#include <iostream>
using namespace std;

void Inventory ::print()
{
    cout << "" << endl;
    cout<<"Items in Store: " <<endl;
    //iterate through BaseSalableItem
    for(int i=0; i<SalableItem.size();i++)
    {
        //print itemType, itemName, itemCost, and itemDescription
        cout <<SalableItem[i]-> getType() << ": " <<" "<<SalableItem[i]->getName()<<" --- "<<SalableItem[i]->getCost()<< " PokeDollars "<<endl;
        cout << "           " << SalableItem[i]->getitemDescription()<<endl;
        cout<< " "<<endl;

    }
}
void Inventory ::addSalableItem(BaseSalableItem* item)
{
    //push back item into Inventory
    SalableItem.push_back(item);
}

vector<BaseSalableItem*> Inventory:: getSalableItem()
{
    //return SalableItem
    return SalableItem;
}

Inventory::~Inventory()
{
    //iterate through SalableItem
    for(int i =0; i<SalableItem.size(); i++)
    {
        //clean up
        delete SalableItem[i];
    }
}