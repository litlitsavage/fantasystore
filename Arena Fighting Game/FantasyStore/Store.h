//
// Created by Sarah Sanders on 11/5/16.
//

#ifndef FANTASYSTORE_STORE_H
#define FANTASYSTORE_STORE_H
#include "Inventory.h"
#include "vector"
#include "Player.h"
/*
 * This class is where the functionality of the store occurs.
 * The only object to the store is the vector cart.
 * There is a getter method for the vector cart
 * addtoBalance will add money to the Player's balance
 * subtractBalance will take money from the Player's balance
 * checkBalance will check the Player's balance
 * There are seven methods regarding the vector cart
 *      addtocart will add a SalableItem from Inventory to the vector cart
 *      removefromcart will remove an item from the cart back to the vector Inventory
 *      checkcartItem will check to see if the items in the cart are SalableItems
 *      checkcartsize will check to see if the vector cart is empty
 *      getcartbalance is a getter method for cart balance
 *      addcartbalance will add the value of the items
 *      subtractcartbalance will subtract the value of the items once an item is taken from the cart
 *
 *
 */


class Store: public Inventory
{
    protected:
        vector<BaseSalableItem*> cart;
        int cartbalance =0;
        Player* thePlayer = Player::instance();

    public:
    /*
     * return vector BaseSalableItem
     */
        vector<BaseSalableItem*> getcart();



    /*
     * if vector cart is empty then print "empty cart"
     * and return 0
     * if not empty, return 1
     */
        bool checkcartsize();


    /*
     * iterate through cart vector
     * and getName of each item
     * print name of item
     */
        void printcart();

    /*
     * destructor
     */
        ~Store();
};


#endif //FANTASYSTORE_STORE_H
