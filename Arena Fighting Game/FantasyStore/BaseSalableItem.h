//
// Created by Sarah Sanders on 11/2/16.
//

#ifndef FANTASYSTORE_BASESALABLEITEM_H
#define FANTASYSTORE_BASESALABLEITEM_H
#include<iostream>
using namespace std;

/*
 * This Base class will hold the objects and methods for all the salable items in the store.
 * Each salable item will have a Type, Name, Description, Cost, and Stat.
 * A constructor is created for the item objects.
 * A getter method for each of the objects will be the only methods for this class.
 */

class BaseSalableItem
{
    //each salable item will have a Type, Name, Description, and Cost, and Stat
    protected:
        string itemType = "";
        string itemName = "";
        string itemDescription = "";
        int cost = 0;
        int Stat = 0;

    public:
    /*
     * create and empty BaseSalableItem
     * by setting the constructors to 0 or an empty string
     */
        BaseSalableItem();


    /*
     * input the type, name, description, cost, and stat
     * of the item into BaseSalableItem
     */
        BaseSalableItem(string itemType, string itemName,string itemDescription, int cost, int Stat);

    /*
     * method that retrieves the data for Type
     */
        string getType();


    /*
    * method that retrieves the data for Name
    */
        string getName();


    /*
    * method that retrieves the data for Description
    */
        string getitemDescription();



    /*
    * method that retrieves the data for Cost
    */
        int getCost();


    /*
     * method that retrieves the data for Stat
    */
        int getStat();

    static BaseSalableItem* create(string itemType, string itemName, string itemDescription, int cost, int Stat);

};


#endif //FANTASYSTORE_BASESALABLEITEM_H
