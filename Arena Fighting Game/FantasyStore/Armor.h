//
// Created by Sarah Sanders on 11/5/16.
//

#ifndef FANTASYSTORE_ARMOR_H
#define FANTASYSTORE_ARMOR_H
#include "BaseSalableItem.h"

/*
 * This class is an item Type and will give Armor Items a defense Value
 * as well as a way to use the Defense Value.
 */
class Armor : public BaseSalableItem
{

    public:
    /*
     * create a constructor that will include the Armor type, name, description, cost and stat
     */
        Armor(string itemType, string itemName,string itemDescription, int cost,int Stat);
};


#endif //FANTASYSTORE_ARMOR_H
