cmake_minimum_required(VERSION 3.6)
project(FantasyStore)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp BaseSalableItem.cpp BaseSalableItem.h Weapon.cpp Weapon.h Armor.cpp Armor.h Health.cpp Health.h Inventory.cpp Inventory.h Player.cpp Player.h Store.cpp Store.h StoreLogic.cpp StoreLogic.h Game.cpp Game.h Enemy.cpp Enemy.h Arena.cpp Arena.h ArenaLogic.cpp ArenaLogic.h GameLogic.cpp GameLogic.h ItemFactory.cpp)
add_executable(FantasyStore ${SOURCE_FILES})