//
// Created by Sarah Sanders on 11/5/16.
//

#ifndef FANTASYSTORE_STORELOGIC_H
#define FANTASYSTORE_STORELOGIC_H
#include "Store.h"




/*
 * This class is the Logic of the Store.
 * It takes in user input and goes through the five main options
 * for the Store: Shop, RemoveItem, Purchase, Sell, Bag, Leave
 */
class StoreLogic: public Store
{
    private:
        string userInput;

    public:

        /*
         * Display Menu Bar that will have 5 options for user selection
         * A for loop will iterate through the 5 options
         * If user input is a-> call beginShopLogic
         *                  b-> call beginCheckoutLogic
         *                  c-> call beingSellLogic
         *                  d-> call viewCartLogic
         *                  e-> call RemoveItemfromCart
         *                  f-> call beginLeaveLogic
         */
        void promptStoreInput();


        /*
         * Print items in store
         * ask for user input which item to add to cart or CANCEL
         * if item is selected,
         * take item from Inventory
         * add item to Cart
         * if YES then loop beginShopLogic
         * if NO then go back to promptuserInput
         */
        void beginShopLogic();




        /*
         * check if there are items in cart
         * if no then print "Empty Cart" and return to promptuserInput
         * if items in cart then print items
         * ask user input "Buy items in Cart?"
         * if NO then return to promptuserInput
         * if YES
         * check Player Balance
         * add item(s) cost in Cart
         * if Player Balance > cost in cart
         * add item(s) to Bag
         * subtract money of item(s) from Player Balance
         * remove item(s) from Cart
         * print item(s) in Bag
         * return to promptuserInput
         * else print "Not enough money"
         * return to promptuserInput
         */
        void beginCheckoutLogic();




        /*
         * check if there are items in Player's Bag
         * if no print "No items in Bag"return to promptuserInput
         * if yes print item(s) in bag
         * ask user input which item(s) to sell or CANCEL
         * if CANCEL return to promptuserInput
         * if item(s) are selected
         * check if user input is valid
         * if no then print "Invalid Input" and loop back to ask user input which item(s) to sell or CANCEL
         * if yes add bag item(s) cost
         * add bag item(s) cost to Player Balance subtract item(s) to Player Bag
         * add item(s) to Store print Bag item(s) print Player Account
         * return to promptuserInput
         */
        void beginSellLogic();



/*
 * check cart size
 * if cart is empty
 *      print "Empty cart"
 *      return to promptuserInput
 * if there are items in cart
 *      print cart
 *      return to promptuserInput */
        void viewCartLogic();


 /*
  * check to see if cart is empty
  * if cart is empty then print empty cart and direct user back to promptuserInput
  * if there are items in cart ask user which items to remove or cancel
  * check user input
  *     if invalid user input print "Bad input" and ask for
  *     if user input is valid then continue
  * if user jnput = cancel then return to prompuserInput
  * if user input = itemName
  *     iterate through the cart vector
  *     change the order of the vector in order for the user input to go to the back of the vector
  *     pop item out of cart
  *     push item in Inventory vector
  *     print cart
  *     print cart balance
  *     return to promptuserInput */
        void beginRemoveItemfromCart();




        /*
         * exit to GameLogic
         */
        void beginLeaveLogic();

};



#endif //FANTASYSTORE_STORELOGIC_H
