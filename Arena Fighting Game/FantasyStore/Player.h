//
// Created by Sarah Sanders on 11/5/16.
//

#ifndef FANTASYSTORE_PLAYER_H
#define FANTASYSTORE_PLAYER_H
#include "Inventory.h"


/*
 * This Player class will include the supplies the user will need for the store.
 * The Player will have a Bag that will be of type Inventory to store the items they buy.
 * Health will not be used in this program, but it will be in the when the Player goes to the Arena
 * The method getBag will retrieve items in the Player's Bag
 * The method subtractPlayerbalance will subtract from the Players balance and return the balance
 * The method addtoBag will add an item of type BaseSalableItem to the Player's Bag
 * The method getbalance will check the Player's balance
 */

class Player
{
    private:
        static Player* inst;
        static vector <BaseSalableItem*> Bag;
        static vector <BaseSalableItem*> BattleBag;
        static string name;
        static int balance;
        static int health;
        static int PlayerAttackValue;
        static int PlayerDefendValue;

        Player();
    public:


        /*
         * create new instance of Player using Singleton method
         */
        static Player* instance();



        /*
         * return Player name
         */
        string getname();



        /*
         * return Player health
         */
        int gethealth();



        /*
         * return Player Attack Value
         */
        int getPlayerAttackValue();



        /*
         * return Player Defend Value
         */
        int getPlayerDefendValue();



        /*
         * return Player balance
         */
        int getbalance();



        /*
         * return Bag of type BaseSalableItem
         */
        vector <BaseSalableItem*> getBag();



        /*
         * return equipped items of type BaseSalableItem
         */
        vector<BaseSalableItem*> getBattleBag();



        /*
         * subtract money from Player balance
         * return param
         */
        void subtractPlayerbalance(int money);



        /*
         * add money to Player balance
         * return param
         */
        void addPlayerbalance(int money);



        /*
         * add Player Attack Stat
         * return param
         */
        void addAttackStat(int stat);



        /*
         * subtract Player Attack Stat
         * return param
         */
        void subtractAttackStat(int stat);



        /*
         * add Player Defend Stat
         * return param
         */
        void addDefendStat(int stat);



        /*
         * subtract Player Defend Stat
         * return return Player Defend Value - param
         */
        void subtractDefendStat(int stat);



        /*
         * subtract Player Health Stat
         * return Player Health Value - param
         */
        void removePlayerhealth(int val);



        /*
         * add Player Health Stat
         * return Player Health Value - param
         */
        void addPlayerhealth(int val);



        /*
         * set Player Health Stat
         * return param
         */
        void setPlayerHealth(int val);



        /*
         * push item of type BaseSalableItem
         * into Player Bag
         */
        void addtoBag(BaseSalableItem *item);



        /*
         * push item of type BaseSalableItem
         * into Player BattleBag
         */
        void addtoBattleBag(BaseSalableItem *item);



        /*
         * print Stats of Player
         * return Health Value, balance, Attack Value,
         * and Defend Value
         */
        void printStats();



        /*
         * print contents of Player Bag
         * return item Name
         */
        void printBag();



        /*
         * print contents of equipped items
         * return item Name
         */
        void printBattleBag();



        /*
         * check if Bag is empty
         * if Bag is empty print "Empty Bag" return 0
         * if Bag is not empty return 1
         */
        bool checkBagsize();



        /*
         * remove item from Player Bag
         * swap param to back of vector
         * and pop_back param
         */
        void removeitemfromBag(int item);



        /*
         * remove item from Player BattleBag
         * swap param to back of vector
         * and pop_back param
         */
        void removeitemfromBattleBag(int item);



        /*
         * return Player Name = param
         */
        void inputPlayerName(string val);



        /*
         * check to see if equipped items vector is empty
         * if empty then print "No Equipped Items" return 0
         * if not empty return 1
         */
        bool checkBattleBagsize();



        /*
         * destructor
         */
        ~Player();

};


#endif //FANTASYSTORE_PLAYER_H
