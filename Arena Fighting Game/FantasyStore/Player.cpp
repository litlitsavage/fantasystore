//
// Created by Sarah Sanders on 11/5/16.
//

#include "Player.h"

Player* Player::inst = 0;
string Player::name= "";
int Player::balance=50;
int Player::health=50;
int Player::PlayerAttackValue=5;
int Player::PlayerDefendValue=5;
vector <BaseSalableItem*> Player:: Bag;
vector <BaseSalableItem*> Player:: BattleBag;





Player::Player()
{
    //return 0
}


Player* Player::instance() {
    if(!inst)       // Example of Lazy Initialization
    {
        //new instance of Player
        inst = new Player();
    }
    //return inst
    return inst;
}


string Player ::getname()
{
    //return name
    return name;
}

int Player ::gethealth()
{
    //return health
    return health;
}

int Player :: getPlayerAttackValue()
{
    //return Attack Value
    return PlayerAttackValue;
}

int Player :: getPlayerDefendValue()
{
    //return Defend Value
    return PlayerDefendValue;
}

int Player ::getbalance()
{
    //return balance
    return balance;
}




vector <BaseSalableItem*> Player ::getBag()
{
    //return Bag
    return Bag;
}

vector <BaseSalableItem*> Player ::getBattleBag()
{
    //return Battle Bag
    return BattleBag;
}


void Player:: subtractPlayerbalance(int money)
{
    //return balance - param
    balance -= money;
}

void Player:: addPlayerbalance(int money)
{
    //return balance + param
    balance += money;
}



void Player ::addAttackStat(int stat)
{
    //return Player Attack Value + param
    PlayerAttackValue += stat;
}



void Player ::subtractAttackStat(int stat)
{
    //return Player Attack Value - param
    PlayerAttackValue -= stat;
}



void Player ::addDefendStat(int stat)
{
    //return Player Defend Value - param
    PlayerDefendValue += stat;
}


void Player ::subtractDefendStat(int stat)
{
    //return Player Defend Value - param
    PlayerDefendValue -= stat;
}



void Player ::removePlayerhealth(int val)
{
    //return health - param
    health-= val;
}


void Player ::addPlayerhealth(int val)
{
    //return health + param
    health+= val;
}

void Player ::setPlayerHealth(int val)
{
    //return health = param
    health = val;
}



void Player::addtoBag(BaseSalableItem *item)
{
    //push param item of type BaseSalable item into Bag
    Bag.push_back(item);
}

void Player::addtoBattleBag(BaseSalableItem *item)
{
    //push param item of type BaseSalableItem into BattleBag
    BattleBag.push_back(item);
}



void Player::printStats()
{
    //use getter methods to print the stats of the Player
    cout<< getname()<<"'s Pokemon Stats: "<< endl;
    cout<<"         "<< gethealth()<<" Health Points "<<endl;
    cout<<"         "<< getPlayerAttackValue()<<" Attack Power "<<endl;
    cout<<"         "<< getPlayerDefendValue()<<" Defense Power "<<endl;
    cout<< " "<< endl;
}




void Player ::printBag()
{
    cout << "" << endl;
    cout<<"Items in Player Bag: " <<endl;
    //iterate through Bag and print itemName
    for(int i=0; i<Bag.size();i++)
    {
        //print item name in Bag
        cout << Bag[i]->getName()<<endl;
    }
}


void Player ::printBattleBag()
{
    cout << "" << endl;
    cout<<"Equipped Items: " <<endl;
    //iterate through Battle Bag and print itemName
    for(int i=0; i<BattleBag.size();i++)
    {
        //print item name in Battle Bag
        cout << BattleBag[i]->getName()<<endl;
    }
}






bool Player ::checkBagsize()
{
    //if nothing in bag then print "empty bag"
    if (Bag.size() ==0)
    {
        cout << "Empty Bag" << endl;
        return 0;
    }
    //if something in Bag then return 1
    return 1;
}




void Player :: removeitemfromBag(int item)
{
    //swap item to the back
    swap(Bag[item], Bag.back());
    //pop_back item
    Bag.pop_back();
}


void Player :: removeitemfromBattleBag(int item)
{
    //swap item to the back of vector
    swap(BattleBag[item], BattleBag.back());
    //pop_back item
    BattleBag.pop_back();
}



void Player ::inputPlayerName(string val)
{
    //Player Name = param
    name=val;
}



bool Player ::checkBattleBagsize()
{
    //if Battle Bag size is 0
    if (BattleBag.size() == 0)
    {
        //print empty cart
        cout<<"Your Pokemon has no equipped items" <<endl;
        cout<<" " <<endl;
        //return 0
        return 0;
    }
    //return 1
    return 1;
}



Player::~Player()
{
    //iterate through Bag vector
    for(int i=0; i<Bag.size(); i++)
    {
        //clean up
        delete Bag[i];
    }
    //iterate through Battle Bag vector
    for(int i=0; i<BattleBag.size();i++)
    {
     //clean up
        delete Bag[i];
    }
}