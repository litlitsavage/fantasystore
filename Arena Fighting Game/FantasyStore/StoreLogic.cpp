//
// Created by Sarah Sanders on 11/5/16.
//

#include "StoreLogic.h"


void StoreLogic ::promptStoreInput()
{
    cout << " -------------------------" << endl;
   cout << "| Welcome to the PokeMart |" << endl;
    cout << " -------------------------" << endl;
    //display player balance
    cout <<"Player balance:" << thePlayer->getbalance()<< " PokeDollars" << endl;
    cout << "Select a Store option below: " << endl;
    cout << " 'a' = Add item to cart"<< endl;
    cout << " 'b' = Proceed to Checkout" << endl;
    cout << " 'c' = Sell item from Bag" << endl;
    cout << " 'd' = View items in cart" << endl;
    cout << " 'e' = Remove item from cart" << endl;
    cout << " 'f' = Leave Store" << endl;

    int validuserInput = 0;

    while(validuserInput == 0)
    {
        //validate the user input.
        cin >> userInput;
        if (userInput == "a" or userInput =="b" or userInput =="c" or userInput=="d" or userInput=="e" or userInput =="f")
        {
            validuserInput = 1;
        }
        else
        {
            cout << "Bad input"<<endl;
        }

    }

    if (userInput == "a")
    {
        beginShopLogic();
    }
    if (userInput == "b")
    {
        beginCheckoutLogic();
    }
    if (userInput == "c")
    {
        beginSellLogic();
    }
    if (userInput == "d")
    {
        viewCartLogic();
    }
    if (userInput == "e")
    {
        beginRemoveItemfromCart();
    }
    if (userInput == "f")
    {
        beginLeaveLogic();
    }

}

void StoreLogic ::beginShopLogic()
{
    //print items in Inventory
     print();
    cout << "'x' to cancel" << endl;
    cout <<""<<endl;
    cout << "Please type the item name above to add to your cart"<<endl;
    //set value for userInput
    int validuserInput=0;
    //create while loop to see if userInput is valid
    while(validuserInput ==0)
    {

        cin >> userInput;
        //check and see if user input = itemName
        for(int i = 0; i < SalableItem.size();i++)
        {
            string x = SalableItem[i]->getName();
            if (userInput == SalableItem[i]->getName())
            {
                validuserInput = 1;
                cout << "Good input" << endl;
                break;
            }

        }
        //if user wants to cancel then return to promptStoreInput
        if (userInput == "x")
        {
            return promptStoreInput();
        }
        //if user input is invalid, ask for another input
        if (validuserInput ==0)
        {
            cout << "Bad input" << endl;
            print();
            cout <<""<<endl;
            cout << "Please type the item name above to add to your cart or CANCEL with 'x'"<<endl;
        }

    }
    //iterate through vector
    for(int i = 0; i < SalableItem.size(); i++)
    {
        //if user input equals an item in Inventory vector
        if(userInput == SalableItem[i]->getName())
        {
            //change the order of the selected item to the last item in vector in order to pop_back
            swap(SalableItem[i], SalableItem.back());
            //use pointer to equate the item the user input item
            BaseSalableItem *x = SalableItem.back();
            //pop_back user input item out of Inventory vector
            SalableItem.pop_back();
            //push user input item into cart vector
            cart.push_back(x);
            //add up cartbalance of items in cart
            cartbalance += x->getCost();
            //print cart
            printcart();
            cout<< "Cart balance is: " << cartbalance << " PokeDollars" <<endl;
            cout<<""<<endl;
            //return to promptStoreinput
            return promptStoreInput();

        }
    }
}

void StoreLogic ::beginCheckoutLogic()
{
    if(checkcartsize()==0)
    {
        cout <<""<<endl;
        return promptStoreInput();
    }
    //print items in cart
    printcart();
    cout <<"Cart balance: "<< cartbalance << " PokeDollars"<<endl;
    cout <<"Player balance: " << thePlayer->getbalance()<< " PokeDollars"<<endl;
    cout <<"    Purchase items in cart? " << endl;
    cout<<"     yes or no?" << endl;
    //check if user input is valid
    int validuserInput=0;
    while (validuserInput ==0)
    {
        cin >> userInput;
        // user wants to add items to cart
        if(userInput == "yes")
        {
            validuserInput = 1;
        }
        //user does not want to add items to cart
        //return to promptStoreInput
        if (userInput == "no")
        {
            return promptStoreInput();
        }

        if (validuserInput==0)
        {
            cout<<"Bad input" << endl;
        }
    }

    if (thePlayer->getbalance() >= cartbalance)
    {
        //subtract cart balance from Player balance
        thePlayer->subtractPlayerbalance(cartbalance);
        //establish an original cart size for vector
        long og_cartsize = cart.size();

        for(int i = 0; i < og_cartsize; i++)
        {
            //pop everything out of cart
            BaseSalableItem* x = cart.back();
            //remove last element in vector
            cart.pop_back();
            //call Player: addtoBag()
            thePlayer-> addtoBag(x);
            cartbalance-=x->getCost();
        }
        //print Player Bag
        thePlayer-> printBag();

        //display player balance
        cout <<"Player balance:" << thePlayer->getbalance()<<" PokeDollars"<<endl;
    }
    else
    {
        cout << "You do not have enough money, peasant. " << endl;
    }
    return promptStoreInput();
}

void StoreLogic ::beginSellLogic()
{
    //if Bag is empty return to promptStoreInput
    if (thePlayer->checkBagsize() == 0)
    {
        cout <<""<<endl;
        return promptStoreInput();
    }
    //print Player Bag
    thePlayer-> printBag();
    cout << "Please select the name of the item to sell" << endl;
    cout << "or CANCEL with 'x'" << endl;
    //set value of user input
    int validuserInput = 0;
    while (validuserInput == 0)
    {
        //ask for user input
        cin >> userInput;
        //check and see if user input = itemName
        for (int i = 0; i < thePlayer->getBag().size(); i++)
        {
            //create a variable for the Name of item in vector Bag
            string x = thePlayer->getBag()[i]->getName();
            //if the user input is correct then print "Good input"
            if (userInput == x)
            {
                //user input is valid
                validuserInput = 1;
                cout << "Good input" << endl;
                //break out of while loop
                break;
            }
        }
        //if user wants to cancel then return to promptStoreInput
        if (userInput == "x")
        {
            //return to promptStoreInput
            return promptStoreInput();
        }
        //if user input is invalid
        if (validuserInput == 0)
        {
            cout << "Bad input" << endl;
            //print cart
            thePlayer->printBag();
            cout << "" << endl;
            cout << "Please select item name above to sell" << endl;
        }
    }


    //iterate through Bag vector
    for(int i = 0; i < thePlayer->getBag().size(); i++)
    {
        //if the user input equals an item name
        if(userInput == thePlayer->getBag()[i]->getName())
        {
            //add the item cost to the player balance
            thePlayer->addPlayerbalance(thePlayer->getBag()[i]->getCost());
            //remove item from Player Bag
            thePlayer->removeitemfromBag(i);
            //print Player Bag
            thePlayer->printBag();
            cout<< "Player balance is: " << thePlayer->getbalance() << " PokeDollars" <<endl;
            cout<<""<<endl;
            //return promptStoreInput
            return promptStoreInput();

        }
    }
}

void StoreLogic ::viewCartLogic()
{
    //if there is nothing in the cart then return to promptStoreInput
    if(checkcartsize()==0)
    {
        cout <<""<<endl;
        return promptStoreInput();
    }
    //if cart contains item(s) then print cart and return to promptStoreInput
    if(checkcartsize()==1)
    {
        //print cart
        printcart();
        //return promptStoreInput
        return promptStoreInput();
    }
}




void StoreLogic ::beginRemoveItemfromCart()
{
    //if cart is empty return to promptStoreInput
    if (checkcartsize()==0)
    {
        return promptStoreInput();
    }

    //print items in cart
    printcart();
    cout << "Please type the the name of the item to remove from cart" << endl;
    cout << "or CANCEL with 'x'" << endl;
    //set value of user input
    int validuserInput = 0;

    //create while loop to see if userInput is valid
    while (validuserInput == 0)
    {
        //ask for user input
        cin >> userInput;
        //check and see if user input = itemName
        for (int i = 0; i < cart.size(); i++)
        {
            //create a variable for the Name of item in vector cart
            string x = cart[i]->getName();
            //if the user input is correct then print "Good input"
            if (userInput == x)
            {
                validuserInput = 1;
                cout << "Good input" << endl;
                //break out of while loop
                break;
            }
        }
        //if user wants to cancel then return to promptStoreInput
        if (userInput == "x")
        {
            //return to promptuserInput
            return promptStoreInput();
        }
        //if user input is invalid
        if (validuserInput == 0)
        {
            cout << "Bad input" << endl;
            //print cart
            printcart();
            cout << "" << endl;
            cout << "Please type the item name above to add to your cart" << endl;
        }
    }

    for(int i = 0; i < cart.size(); i++)
    {
        if(userInput == cart[i]->getName())
        {
            //change the order of the selected item to the last item in vector in order to popback
            swap(cart[i], cart.back());
            //move items to back of vector
            BaseSalableItem *x = cart.back();
            //take items out of cart
            cart.pop_back();
            //put items in Inventory
            SalableItem.push_back(x);
            //subtract balance of cart
            cartbalance -= x->getCost();
            //print items in cart
            printcart();
            cout << "" <<endl;
            //print cart balance
            cout<< "Cart balance is: " << cartbalance <<endl;
            cout << ""<<endl;
            //return to promptStoreInput
            return promptStoreInput();

        }
    }

}

void StoreLogic ::beginLeaveLogic()
{
   //exit store
}
