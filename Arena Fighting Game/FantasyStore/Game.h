//
// Created by Sarah Sanders on 12/2/16.
//

#ifndef FANTASYSTORE_GAME_H
#define FANTASYSTORE_GAME_H
#include "StoreLogic.h"
#include "ArenaLogic.h"
#include "Player.h"

/*
 * This file creates instances necessary for the entirety
 * of the program. Store, Arena and Player instances are created and implemented
 * in order to be called on throughout the Logic of the Game.
 */
class Game {
    protected:
        StoreLogic *StoreLogic1 = new StoreLogic;
        ArenaLogic *ArenaLogic1 = new ArenaLogic;
        Player* thePlayer = Player::instance();

    public:
        /*
         * check to see if save file can be opened
         * if not, then print "file cannot be opened"
         * if it can be opened, output any new data
         * onto data file
         */
        void save();


        /*
         * getter method to retrieve instance of Store
         */
        StoreLogic* getStoreLogic();

        /*
         * getter method to retrieve instance of Arena
         */
        ArenaLogic* getArenaLogic();

        /*
         * destructor for cleanup and return 0
         */
        ~Game();

};


#endif //FANTASYSTORE_GAME_H
