//
// Created by Sarah Sanders on 12/2/16.
//

#ifndef FANTASYSTORE_ENEMY_H
#define FANTASYSTORE_ENEMY_H
#include <iostream>
#include "vector"
using namespace std;



class Enemy {
    private:
        string name;
        int health;
        int attack;
        int defense;
        int balance;
        bool attackState;
        bool defendState;

    public:

        /*
         * include parameters name of type string and health, attack, defense, and balance
         * all of type int
         */
        Enemy(string name, int health, int attack, int defense, int balance);


        /*
         * create a getter method to retrieve Enemy name
         */
        string getName();


        /*
         * create a getter method to retrieve the health value of Enemy
         */
        int getHealth();


        /*
         * create a getter method to retrieve the attack value of Enemy
         */
        int getAttack();


        /*
         * create a getter method to retrieve the defense value of Enemy
         */
        int getDefense();


        /*
         * create a getter method to retrieve the balance of Enemy
         */
        int getBalance();


        /*
         * Set Attack State to true
         */
        void setAttackState(bool value);


        /*
         * set Defend State to true
         */
        void setDefendState(bool value);

        /*
         * getter method to retrieve Attack State
         */
        bool getAttackState();

        /*
         * subtract the Enemy health by using parameter int val
         */
        void removeEnemyHealth(int val);


};


#endif //FANTASYSTORE_ENEMY_H
