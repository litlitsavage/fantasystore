//
// Created by Sarah Sanders on 12/2/16.
//

#include "Enemy.h"


Enemy::Enemy(string name, int health, int attack, int defense, int balance) {
    //Enemy name is type string
    this->name = name;
    //Enemy health is type int
    this-> health = health;
    //Enemy attack is type int
    this->attack = attack;
    //Enemy defense is type int
    this->defense = defense;
    //Enemy balance is type int
    this->balance = balance;
}

string Enemy::getName()
{
    //return name
    return name;
}

int Enemy::getHealth()
{
    //return health
    return health;
}

int Enemy::getAttack() {
    return attack;
}

int Enemy::getDefense()
{
    //return defense
    return defense;
}


int Enemy::getBalance()
{
    //return balance
    return balance;
}


void Enemy::setAttackState(bool value)
{
    //return true or false
    attackState=value;
}

void Enemy::setDefendState(bool value)
{
    //return true or false
    defendState=value;
}

bool Enemy::getAttackState()
{
    //return attack State
    return attackState;
}


void Enemy::removeEnemyHealth(int val)
{
    //subtract health
    health-=val;
}


