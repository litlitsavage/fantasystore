//
// Created by Sarah Sanders on 12/2/16.
//

#include "Arena.h"
#include <iostream>
using namespace std;



bool Arena ::checkEnemy()
{
    //if nothing in EnemyVector then print "empty bag"
    if (EnemyVector.size() ==0)
    {
        //cout "Player wins game"
        cout << "Congratulations! You have defeated all the Gym Leaders! You are now the Pokemon Champion!" << endl;
        exit(1);
    }
    //if something in Bag then return 1
    return 1;
}


void Arena::addEnemy(Enemy* enemy)
{
    //push back enemy instance into EnemyVector
    EnemyVector.push_back(enemy);
}

void Arena::removeEnemy()
{
    //erase enemy instance from EnemyVector by using .erase()
    EnemyVector.erase(EnemyVector.begin()+0);
}



Arena::~Arena()
{
    for(int i=0; EnemyVector.size(); i++)
    {
        delete EnemyVector[i];
    }
}