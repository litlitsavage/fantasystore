//
// Created by Sarah Sanders on 12/2/16.
//

#include "GameLogic.h"


void GameLogic::StartGameLogic()
{
    cout <<" a = Start New Game"<<endl;
    cout <<" b = Continue Saved File"<<endl;
    //create variable to validate user input
    int validuserInput = 0;

    //while the user input variable is 0
    while(validuserInput == 0)
    {
        //ask user input
        cin >> userInput;

        //if user input is 'a' or 'b'
        if (userInput == "a" or userInput =="b")
        {
            //validuserInput = 1
            validuserInput = 1;
        }
        else
        {
            //print "Bad "Input" to prompt user
            cout << "Bad input"<<endl;
        }

    }
    //if user input = 'a'
    if (userInput == "a")
    {
        //ask Player's Name
        askPlayerName();
    }
    if (userInput == "b")
    {
        //promptGameInput
        promptGameInput();
    }
}



void GameLogic::askPlayerName()
{
    cout<<"What is your name?"<<endl;
    //ask user input
    cin>>userInput;
    //set user input as param for inputPlayerName
    thePlayer->inputPlayerName(userInput);
    cout<<" "<<endl;
    cout<<" "<<endl;
    cout<<" "<<endl;
    cout<< "Welcome "<<thePlayer->getname()<<" to the POKEMON ARENA GAME"<<endl;
    cout<< "Instructions: "<<endl;
    cout<< "You are a Pokemon Trainer looking to become the next Pokemon Champion. "<<endl;
    cout<< "You must battle other Gym Leaders' Pokemon with your own in the Pokemon Arena."<<endl;
    cout<< "Your Pokemon has Health Points,"<<endl;
    cout<< "an Attack Value that does damage to the opponent,"<<endl;
    cout<< "and a Defense Value that defends itself from some of the opponent attack."<<endl;
    thePlayer->printStats();
    cout<< "You can strengthen your Pokemon's stats buy purchasing Weapon and Armor items"<<endl;
    cout<< "from the Store and equipping your Pokemon with them."<<endl;
    cout<< "You can also purchase Health items that will restore Pokemon Health in battle."<<endl;
    cout<< "If you defeat a Gym Leader, your Pokemon's Health will reset to 50, and you get a reward"<<endl;
    cout<< "in Pokedollars that you can use to purchase more items from the Store and making your Pokemon even stronger."<<endl;
    cout<< "If you lose to a Gym Leader, your Pokemon Health will reset to 50, and your will be redirected to the Main Menu."<<endl;
    cout<< "Once all the Gym Leaders are defeated you will be declared the Pokemon Champion and win the game!"<<endl;
    cout<<" "<<endl;
    cout<<" "<<endl;
    cout<<" "<<endl;


    //return promptGameInput()
    return promptGameInput();
}


void GameLogic::promptGameInput()
{
    cout<< "Welcome "<<thePlayer->getname()<<" to the POKEMON ARENA GAME"<<endl;
    cout << "Select a Game option below: " << endl;
    cout << " 'a' = View list of Opponents"<< endl;
    cout << " 'b' = Fight in Arena"<< endl;
    cout << " 'c' = Go to Store" << endl;
    cout << " 'd' = Equip Pokemon with Battle Item" << endl;
    cout << " 'e' = Remove Battle Item from Pokemon" << endl;
    cout << " 'f' = Print Pokemon Stats" << endl;
    cout << " 'g' = Quit Game" << endl;


    //create variable to validate user input
    int validuserInput = 0;

    //while the user input variable is 0
    while(validuserInput == 0)
    {
        //ask user input
        cin >> userInput;
        //if user input is 'a', 'b', 'c', 'd', 'e', 'f', or 'g'
        if (userInput == "a" or userInput =="b" or userInput =="c" or userInput=="d" or userInput=="e" or userInput =="f" or userInput == "g")
        {
            //validuserInput = 1
            validuserInput = 1;
        }
        //if invalid input
        else
            //print "Bad Input" if to prompt user input
        {
            cout << "Bad Input"<<endl;
        }

    }
    //if user input = 'a'
    if(userInput=="a")
    {
        cout <<"List of Opponents"<<endl;
        //print List of Enemies
        viewEnemyList();

        //return promptGameInput()
        return promptGameInput();
    }
    //if user input = 'b'
    if (userInput == "b")
    {
        //return beginArenaLogic()
        return beginArenaLogic();
    }
    //if user input = 'c'
    if (userInput == "c")
    {
        //return beginStoreLogic()
        return beginStoreLogic();
    }
    //if user input = 'd'
    if (userInput == "d")
    {
        //return beginEquipLogic()
        return beginEquipLogic();
    }
    //if user input = 'e'
    if (userInput == "e")
    {
        //return beginUnequipLogic()
        return beginUnequipLogic();
    }
    //if user input = 'f'
    if (userInput == "f")
    {
        //return beginPrintPlayerStatsLogic()
        return beginPrintPlayerStatsLogic();
    }
    //if user input = 'g'
    if (userInput == "g")
    {
        //return beginQuitLogic()
        return beginQuitLogic();
    }

}



void GameLogic::viewEnemyList()
{
    //call getEnemyList() function in Arena
   ArenaLogic1->getEnemyList();

    //return promptGameInput()
    return promptGameInput();
}




void GameLogic::beginArenaLogic()
{
    //call configureEnemyMove() function in Arena
    ArenaLogic1->configureEnemyMove();

    //call checkEnemy(); function in Arena
    ArenaLogic1->checkEnemy();

    //return promptGameInput()
    return promptGameInput();
}

void GameLogic::beginStoreLogic()
{
    //call promptStoreInput() in Store
    StoreLogic1->promptStoreInput();

    //return promptGameInput()
    return promptGameInput();
}

void GameLogic::beginEquipLogic()
{
    //check if item is weapon or armor
    bool checkitem = 0;
    //while loop to establish the item type
    while (checkitem == 0)
    {
        //iterate through Bag to only print out the items that are type Weapon and Armor
        for (long i = 0; i < thePlayer->getBag().size(); i++)
        {
            //if item type is a Weapon then break out of while loop
            if (thePlayer->getBag()[i]->getType() == "Weapon")
            {
                checkitem = 1;
                break;
            }
            //if item type is an Armor then break out of while loop
            if (thePlayer->getBag()[i]->getType() == "Armor")
            {
                checkitem = 1;
                break;
            }

        }
        //if item is neither Weapon or Armor then return to promptGameInput
        if (checkitem == 0)
        {
            //No items to equip if checkitem = 0
            cout << "There are no items to equip your Pokemon" << endl;
            cout << "" << endl;
            promptGameInput();
        }
    }
    cout << "" << endl;
    cout << "Choose an item to equip your Pokemon " << endl;
    cout << "or CANCEL with 'x' " << endl;
    cout << "" << endl;
    //iterate through Bag
    for (int i = 0; i < thePlayer->getBag().size(); i++)
    {
        //if item is Weapon then print Name, Description, and Attack Value
        if (thePlayer->getBag()[i]->getType() == "Weapon")
        {
            cout << thePlayer->getBag()[i]->getName() <<"   Attack Value: "<< thePlayer->getBag()[i]->getStat()<<" Damage Points"<<endl;

        }
            //if item is Armor then print Name, Description, and Defend Value
        else if (thePlayer->getBag()[i]->getType() == "Armor")
        {
            cout << thePlayer->getBag()[i]->getName()<<"    Defense Value: "<< thePlayer->getBag()[i]->getStat()<<" Defense Points"<<endl;
        }
    }
    cout << "" << endl;
    //create value for user input
    int validuserInput = 0;
    //while loop to validate user input
    while (validuserInput == 0)
    {
        //ask for user input
        cin >> userInput;
        //iterate through Bag to validate the user input
        for (int i = 0; i < thePlayer->getBag().size(); i++)
        {
            //create a variable for the Name of item in vector Bag
            string x = thePlayer->getBag()[i]->getName();
            //if the user input is valid add values of items to Player stats
            if (userInput == x)
            {
                //if item is type Weapon then add Attack Value to PlayerAttackValue
                if(thePlayer->getBag()[i]->getType()=="Weapon")
                {
                    thePlayer->addAttackStat(thePlayer->getBag()[i]->getStat());
                    validuserInput =1;
                    cout<<""<<endl;
                    break;
                }
                //if item is type Weapon then add Defend Value to PlayerDefendValue
                if(thePlayer->getBag()[i]->getType()=="Armor")
                {
                    thePlayer->addDefendStat(thePlayer->getBag()[i]->getStat());
                    validuserInput =1;
                    cout<<""<<endl;
                    break;
                }
            }

        }
        //if user wants to cancel then return to promptGameInput
        if (userInput == "x")
        {
            //return to promptGameInput
            cout<<" "<<endl;
            return promptGameInput();
        }
        //if user input is invalid then print Bad Input
        if (validuserInput == 0)
        {
            cout << "Bad input" << endl;
        }
    }
        //iterate through Bag to remove user input from Bag and add it to Battle Bag
        for(int i = 0; i < thePlayer->getBag().size(); i++)
        {
            //if user input equals item name, remove item from Bag and push it into Battle Bag
            if (userInput == thePlayer->getBag()[i]->getName())
            {
                //create a variable for iterater of type BaseSalableItem
                BaseSalableItem item = *thePlayer->getBag()[i];
                //remove item from Bag
                thePlayer->removeitemfromBag(i);
                //add item to BattleBag
                thePlayer->addtoBattleBag(&item);
                //WHEN I ADD MORE THAN ONE ITEM IT WILL ALWAYS PRINT OUT THE FIRST ITEM
                cout << "Your Pokemon has been equipped with " << userInput <<endl;
                cout<< " " <<endl;
                break;
            }
        }
    return promptGameInput();
}




void GameLogic::beginUnequipLogic()
{

    //if Battle Bag is empty then return to promptGameInput
    if(thePlayer->checkBattleBagsize()==0)
    {
        return promptGameInput();
    }
    //print equipped items
    thePlayer->printBattleBag();
    cout << "Please choose the Battle Items to remove from your Pokemon"<<endl;
    cout << "or CANCEL with 'x'" << endl;

    //set value of user input
    int validuserInput = 0;

    //create while loop to check valid user input
    while(validuserInput==0)
    {
        //ask for user input
        cin >> userInput;

        //iterate through Battle Bag to validate user input
        for (int i = 0; i< thePlayer->getBattleBag().size() ; i++)
        {
            //create a variable for the item name in Battle Bag
            string x = thePlayer->getBattleBag()[i]->getName();
            //if user input is valid then break out of while loop
            if (userInput == x)
            {
                //if the item type is Weapon
                if (thePlayer->getBattleBag()[i]->getType() == "Weapon")
                {
                    //subtract Player Attack Stat using param item Stat
                    thePlayer->subtractAttackStat(thePlayer->getBattleBag()[i]->getStat());

                    //validate user input
                    validuserInput = 1;
                    cout << "" << endl;

                    //break out of while loop
                    break;
                }
                //if item type is Armor
                if (thePlayer->getBattleBag()[i]->getType() == "Armor")
                {
                    //subtract Player Defend Value using param item Stat
                    thePlayer->subtractDefendStat(thePlayer->getBattleBag()[i]->getStat());

                    //validate user input
                    validuserInput = 1;
                    cout << "" << endl;

                    //break out of while loop
                    break;
                }
            }
        }
            //if user input = 'x'
            if (userInput == "x")
            {
                //return to promptGameInput
                return promptGameInput();
            }

            //if user input invalid
            if (validuserInput == 0)
            {
                cout << "Bad Input" << endl;

                //print equipped items asking for user input
                thePlayer->printBattleBag();
                cout << "Please choose the Battle Items to remove from your Pokemon" << endl;
                cout << "or CANCEL with 'x'" << endl;
            }
        }

    for (int i = 0; i < thePlayer->getBattleBag().size(); i++)
    {
        //if user input equals item name, remove item from Battle Bag and push it into Bag
        if (userInput == thePlayer->getBattleBag()[i]->getName())
        {
                        //create a variable for iterater of type BaseSalableItem
                        BaseSalableItem item = *thePlayer->getBattleBag()[i];
                        //remove item from Battle Bag
                        thePlayer->removeitemfromBattleBag(i);
                        //add item to Bag
                        thePlayer->addtoBag(&item);
                        //print Player Battle Bag
                        thePlayer->printBattleBag();
        }
    }
    //return promptGameInput()
    return promptGameInput();
}

void GameLogic::beginPrintPlayerStatsLogic()
{
    //call printStats() function in Player
    thePlayer->printStats();

    //return promptGameInput()
    return promptGameInput();
}

void GameLogic::beginQuitLogic()
{
    //exit code
}