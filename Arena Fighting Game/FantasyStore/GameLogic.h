//
// Created by Sarah Sanders on 12/2/16.
//

#ifndef FANTASYSTORE_GAMELOGIC_H
#define FANTASYSTORE_GAMELOGIC_H
#include <iostream>
using namespace std;
#include "Game.h"


/*
 * This class is the structure of the Game, Store, and Arena
 * and determines how each instance uses the Player, Enemies,
 * and Items. Determining the user input, it will call upon
 * functions that will allow the Player to go to the Store and fight
 * in the Arena while using items purchased from the Store.
 * The Player can equip and unequip items for the Arena.
 */


class GameLogic: public Game
{
    private:
        string userInput;

    public:


        /*
         * I could not get to save and upload
         * a saved file to my program.
         */
        void StartGameLogic();


        /*
         * Print  options
         * Ask for user input. Set user input
         * as param for inputPlayerName()
         * return promptGameInput()
         */
        void askPlayerName();


        /*
         * This method will contain options for the Player
         * These options include View Opponents, Arena, Store, Equip,
         * Unequip, Stats and Quit
         */
        void promptGameInput();


        /*
         * Call the getEnemyList() function in Arena
         * Return promptGameInput()
         */
        void viewEnemyList();


        /*
         * Call configureEnemyMove() function in Arena
         * Call checkEnemy() function in Arena
         * Return promptGameInput()
         */
        void beginArenaLogic();



        /*
         * Call promptStoreInput() function in Store
         * Return promptGameInput()
         */
        void beginStoreLogic();



        /*
         * Check to see if there are any equippable items in Player Bag
         * If there are no items of type Weapon or Armor then
         * print "No Equippable items" and return promptGameInput().
         * If there is an item of type Weapon or Armor then print out items.
         * Ask for user input of what item they want to equip. Validate
         * user input. If invalid user input print "Bad Input" and prompt for user
         * input. If valid user input and user input = 'x', return promptGameInput().
         * If user input = itemName iterate through Bag and add Weapon item Stat
         * to Player Attack Value, or Defense item Stat to Player Defense Value.
         * Remove item from Player Bag and add to Player Battle Bag. Print added item.
         * Return promptGameInput().
         */
        void beginEquipLogic();



        /*
         * Check if Player Battle Bag is empty. If empty then print
         * "No Equipped items" and return promptGameInput(). If there
         * are equipped items then print out Battle Bag. Ask for user input
         * of what item they want to unequip. Validate
         * user input. If invalid user input print "Bad Input" and prompt for user
         * input. If valid user input and user input = 'x', return promptGameInput().
         * If user input = itemName iterate through Battle Bag and subtract Weapon item Stat
         * from Player Attack Value, or Defense item Stat from Player Defense Value.
         * Remove item from Player Battle Bag to Player Bag. Print Player Battle Bag.
         */
        void beginUnequipLogic();



        /*
         * This will allow the Player to view their name,
         * health, equipped items, and balance
         */
        void beginPrintPlayerStatsLogic();



        /*
         * This will exit the code
         */
        void beginQuitLogic();


};


#endif //FANTASYSTORE_GAMELOGIC_H
