//
// Created by Sarah Sanders on 11/5/16.
//

#ifndef FANTASYSTORE_HEALTH_H
#define FANTASYSTORE_HEALTH_H
#include "BaseSalableItem.h"
/*
 * This class is an item Type and will hold all the constructors a Health item needs.
 * This includes type, name, description, cost and Stat.
 */

class Health: public BaseSalableItem
{
    public:
    /*
     * create a constructor that will include the Health type, name, description, cost and Stat
     */
        Health(string itemType, string itemName,string itemDescription, int cost,int Stat);
};


#endif //FANTASYSTORE_HEALTH_H
