//
// Created by Sarah Sanders on 11/5/16.
//

#include "Armor.h"



Armor::Armor(string itemType, string itemName,string itemDescription, int cost,int Stat)
{
    //name the item type
    this->itemType = itemType;
    //name the item name
    this->itemName = itemName;
    //name the item description
    this->itemDescription = itemDescription;
    //name the item cost
    this->cost = cost;
    //name the item stat
    this->Stat= Stat;
}