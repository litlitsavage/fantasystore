//
// Created by Sarah Sanders on 11/5/16.
//

#ifndef FANTASYSTORE_INVENTORY_H
#define FANTASYSTORE_INVENTORY_H
#include "vector"
#include "BaseSalableItem.h"
#include <iostream>
using namespace std;

/*
 * This class will be derived from BaseSalableItem because it encompasses a salable item and
 * stores the items in a vector.
 * This vector is where the items are stored once they are created.
 * checkSalableItem will check to see if item selected is in vector SalableItem
 * Items are added to the vector SalableItem by the method addSalableItem
 */

class Inventory
{
    protected:
        vector<BaseSalableItem*> SalableItem;
    public:

        /*
         * method prints the items in Inventory
         * by iterating through the vector
         */
        void print();

        /*
         * push item of type BaseSalableItem into Inventory vector
         */
        void addSalableItem(BaseSalableItem* item);


        /*
         * return SalableItem
         */
        vector<BaseSalableItem*> getSalableItem();



        /*
         * destructor
         */
        ~Inventory();
};


#endif //FANTASYSTORE_INVENTORY_H
