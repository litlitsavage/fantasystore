//
// Created by Sarah Sanders on 12/10/16.
//

#include <iostream>
#include "BaseSalableItem.h"
#include "Armor.h"
#include "Weapon.h"
#include "Health.h"

BaseSalableItem* BaseSalableItem::create(string itemType, string itemName, string itemDescription, int cost, int Stat)
{
    //if the item is type Weapon
    if(itemType== "Weapon")
        //return new instance of Weapon
        return new Weapon(itemType,itemName,itemDescription,cost,Stat);


        //if the item is type Armor
    else if(itemType=="Armor")
        //return new instance of Armor
        return new Armor(itemType,itemName,itemDescription,cost,Stat);


    //if the item is type Health
    else if (itemType=="Health")
        //return new instance of Health
        return new Health(itemType,itemName,itemDescription,cost,Stat);

    //return NULL
    return NULL;
}